<?php
$trustedDomains = getenv('TRUSTED_DOMAINS');
if ($trustedDomains) {
    $CONFIG['trusted_domains'] = array_filter(array_map('trim', explode(' ', $trustedDomains)));
}
