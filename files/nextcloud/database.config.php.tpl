<?php
$CONFIG = array(
    'dbname' => getenv('DB_NAME'),
    'dbhost' => getenv('DB_HOST'),
    'dbport' => getenv('DB_PORT'),
    'dbtableprefix' => getenv('DB_PREFIX'),
    'dbuser' => getenv('DB_USER'),
    'dbpassword' => getenv('DB_PASSWORD'),
);
