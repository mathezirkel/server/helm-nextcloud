#!/bin/sh

/var/www/html/occ app:install notify_push
{{- if .Values.ingress.enabled }}
/var/www/html/occ config:app:set notify_push base_endpoint --value="https://{{ .Values.ingress.host }}/push"
# /var/www/html/occ notify_push:setup "https://{{ .Values.ingress.host }}"
{{- else }}
/var/www/html/occ config:app:set notify_push base_endpoint --value="http://{{ printf "%s-nginx" (include "nextcloud.fullname" .) }}/push"
# /var/www/html/occ notify_push:setup "http://{{ printf "%s-nginx" (include "nextcloud.fullname" .) }}/push"
{{- end }}
