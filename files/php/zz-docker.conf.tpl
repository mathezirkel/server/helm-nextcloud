[global]
daemonize = no

[www]
listen = 9000
pm.max_children = ${PM_MAX_CHILDREN}
pm.start_servers = ${PM_START_SERVERS}
pm.min_spare_servers = ${PM_MIN_SPARE_SERVERS}
pm.max_spare_servers = ${PM_MAX_SPARE_SERVERS}
pm.max_requests = ${PM_MAX_REQUESTS}
