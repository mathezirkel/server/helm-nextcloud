# Nextcloud Helm chart

{{ template "chart.deprecationWarning" . }}

{{ template "chart.badgesSection" . }}

This repository contains a [Helm](https://helm.sh/) chart to deploy [Nextcloud](https://nextcloud.com/) on Kubernetes.
Nextcloud is a self-hosted, open-source file sync and share platform that allows you to securely access and share your files, calendars, contacts, and more.

We use the official image of [Nextcloud](https://hub.docker.com/_/nextcloud/), specifically the [NGINX](https://nginx.org/) flavored version.

The chart requires the following resources:

* External resources:
    * An existing [PostgreSQL](https://www.postgresql.org/) database.
    * An existing [restic](https://restic.net/) repository accessible via sftp (if backup is enabled)
* In-cluster components:
    * [Traefik v2](https://traefik.io/traefik/) with its associated CRDs (if ingress is enabled)
    * [Argo Workflows](https://argoproj.github.io/workflows/) with its associated CRDs (if backup is enabled)

{{ template "chart.maintainersSection" . }}

{{ template "chart.sourcesSection" . }}

{{ template "chart.requirementsSection" . }}

## Installation and configuration

1. Create namespace.

    ```shell
    kubectl create namespace nextcloud
    ```

2. Create a database user and a database in the internal database using the [script](./database-setup/database-setup.sql). Don't forget to modify the password.

3. Create a secret for the nextcloud database crendentials:

    ```shell
    kubectl create secret generic database-credentials \
    --from-literal=database=nextcloud \
    --from-literal=user=nextclouduser \
    --from-literal=password=CHANGEME \
    -n nextcloud
    ```

4. Create a secret for the mail credentials:

    ```shell
   kubectl create secret generic mail-credentials \
   --from-literal=host=CHANGEME \
   --from-literal=port=CHANGEME \
   --from-literal=encryptionMode=CHANGEME \
   --from-literal=username=CHANGEME \
   --from-literal=password=CHANGEME \
   --from-literal=fromLocalPart=CHANGEME \
   --from-literal=fromDomain=CHANGEME \
   --from-literal=toEmail=CHANGEME \
   -n nextcloud
   ```

5. Optionally create a secret for the valkey credentials:

    ```shell
    kubectl create secret generic nextcloud-valkey \
    --from-literal=valkey-password=CHANGEME \
    -n nextcloud
    ```

    **Note**: The sign '@' is not allowed in the password string.


6. Optionally create a secret for the metrics token:

   ```shell
   kubectl create secret generic nextcloud-metrics \
   --from-literal=token=CHANGEME \
   -n nextcloud
   ```

7. Deploy the helm chart:

   ```shell
   helm upgrade --install -n nextcloud nextcloud .
   ```

8. Change the ownership of config and data directory and create `CAN_INSTALL` inside the nextcloud container.

    ```shell
    chown www-data:www-data /var/www/html/config
    chown www-data:www-data <dataDir>
    touch /var/www/html/config/CAN_INSTALL
    ```

9. Install Nexcloud by going to the website and following instructions.

10. Handle nextcloud warnings.

11. Install apps.
    * Calendar
    * Contacts
    * Group folder
    * File access control
    * Client push
    * Social Login
    * Login Notes
    * Tasks
    * External Sites

12. Setup High Performance File Backend

    If `autoSetup` is not enabled, configure the settings manually within the Nextcloud container.

    ```shell
    su -s /bin/sh www-data
    /var/www/html/occ app:install notify_push
    /var/www/html/occ config:app:set notify_push base_endpoint --value="https://cloud.mathezirkel-augsburg.de/push"
    ```

13. Setup metrics exporter

    If `autoSetup` is not enabled, configure the settings manually within the Nextcloud container.

    ```shell
    su -s /bin/sh www-data
    /var/www/html/occ config:app:set serverinfo token --value "$NEXTCLOUD_METRICS_TOKEN"
    ```

14. Configure the Nextcloud.

## Set up backup

1. Initialize a restic repository.

    ```shell
    restic -r sftp:<ssh-alias>:<path-to-repository> init
    ```

2. Create a secret for the repository credentials.

    ```shell
    kubectl create secret generic backup-restic-credentials \
    --from-literal=password=CHANGEME \
    -n nextcloud
    ```

3. Create a secret for the SSH Key.

    ```shell
    ssh-keyscan -t rsa,ecdsa,ed25519 -H -p CHANGEME CHANGEME > /tmp/known_hosts
    ```

    ```shell
    kubectl create secret generic backup-ssh-credentials \
    --from-literal=hostname=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=user=CHANGEME \
    --from-literal=path=CHANGEME \
    --from-file=knownHosts=/tmp/known_hosts \
    --from-file=privateKey=CHANGEME \
    -n nextcloud
    ```

4. Test your backup regularly! Detailed instructions can be found [here](https://gitlab.com/mathezirkel/server/helm-nextcloud/-/tree/main/backup/).

## Update notes

**Do not use the web-based updater and do not update via occ!**

Updating is done by replacing the old images in the manifests and rollouting the updated deployments.

**It is only possible to upgrade one major version at a time.**

Since all data and configuration is stored in persistent volumes, nothing gets lost. The startup script will check for the version in the volume and the installed version of the docker image. If it finds a mismatch, it will automatically starts the upgrade process.

## Maintenance

In order to use the [occ command](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#config-commands-label) execute

```shell
su -s /bin/sh www-data
```

inside the nextcloud container.

{{ template "chart.valuesSection" . }}

## License

This repository is based on the work from the [Nextcloud helm chart](https://github.com/nextcloud/helm) which is licensed under the [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.txt).
The modifications and contents of this repository are licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](LICENSE).

{{ template "helm-docs.versionFooter" . }}
