<?php
    $CONFIG = array (
        'dbname' => getenv('POSTGRES_DB'),
        'dbhost' => getenv('POSTGRES_HOST'),
        'dbport' => 5432,
        'dbuser' => getenv('POSTGRES_USER'),
        'dbpassword' => getenv('POSTGRES_PASSWORD'),
    );
