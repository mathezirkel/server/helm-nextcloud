# Backups

## Manually create a backup

```shell
argo submit --from workflowtemplate/nextcloud-backup -n nextcloud
```

## Test a backup

1. Pull a snapshot from the restic repository.

    ```shell
    restic -r CHANGEME restore latest --target ./backup_io/
    ```

2. Prepare the backup files.

    ```shell
    chown -R 82:82 backup/backup_io/backup/web
    chmod -R 775 backup/backup_io/backup/web
    chown -R 82:82 backup/backup_io/backup/data
    chmod -R 775 backup/backup_io/backup/data
    ```

3. Start the dev setup.

    ```shell
    docker compose up
    ```

    The dev setup automatically restores the `.sql` file from the backup. If you also want to test the `.dump` file, execute

    ```shell
    docker compose run --rm database pg_restore -U nextclouduser --clean -d databasename /backup_io/nextcloud.dump
    ```

4. The Nextcloud instance is available under [localhost:8080](localhost:8080).
You need a valid local account to login and check files.
