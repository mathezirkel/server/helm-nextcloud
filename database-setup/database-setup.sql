-- Add user for postgresql
CREATE USER nextclouduser WITH PASSWORD 'CHANGEME';

-- Add database for nextcloud
CREATE DATABASE nextcloud WITH OWNER=nextclouduser;