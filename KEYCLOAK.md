# Login via Keycloak

## Keycloak settings

1. Add new client
    1. General Settings

        | Attribute | Value |
        |--|--|
        | Client type | OpenIDConnect |
        | Client ID | nextcloud-auth |
        | Name | Nextcloud Authentification |
        | Description | |
        | Always display in console | true |

    2. Capability config
        * All options: true

2. Clients -> nextcloud-auth

    | Attribute | Value |
    |--|--|
    | Root URL | https://cloud.mathezirkel-augsburg.de |
    | Home URL | https://cloud.mathezirkel-augsburg.de |
    | Valid redirect URIs | https://cloud.mathezirkel-augsburg.de/* |
    | Web origins | https://cloud.mathezirkel-augsburg.de |

3. Clients -> nextcloud-auth -> roles
    * Add a new role `nextcloud-admin` and assign it to the group `admin`.

4. Clients -> nextcloud-auth -> Client scopes -> nextcloud-auth-dedicated
    1. Add new mapper (From predefined mapper) `client-roles`

        | Attribute | Value |
        |--|--|
        | Token claim name | resource_access.${client_id}.roles |
        | Claim JSON Type | String |
        | Add to userinfo | true |

    2. Add new mapper (By configuration) `User Property`

        | Attribute | Value |
        |--|--|
        | name | friendly_name |
        | Property | username |
        | Token claim name | sub |
        | Claim JSON Type | String |
        | Add to userinfo | true |

    3. Add new mapper (By configuration) `Custom full name`

        | Attribute | Value |
        |--|--|
        | name | custom_full_name |
        | Property | Custom User's full name |
        | Token claim name | custom_full_name |
        | Claim JSON Type | String |
        | Add to userinfo | true |

5. Clients -> nextcloud-auth -> Client scopes -> nextcloud-auth-dedicated -> Scope
    * Disable `Full scope allowed`.

6. Realm settings -> OpenID Endpoint Configuration

### Nextcloud settings

1. Install the app `Social Login`.

2. Enable the following settings.
    * Benutzerprofil nach jeder Anmeldung aktualisieren.
    * Verhindern, dass sich Benutzer ohne gemappte Gruppe anmelden können.

3. Add a new "Benutzerdefinierte OpenID Connect Anbindung"

    | Attribute | Value |
    |--|--|
    | Internal name | keycloak-openid |
    | Display Name | Keycloak OpenID Connection |
    | Authorize-URL | see 6 above |
    | Token URL | see 6 above |
    | Display name claim | custom_full_name |
    | Userinfo-URL | see 6 above |
    | Logout URL | see 6 above |
    | Client ID | nextcloud-auth |
    | Client secret | Clients -> nextcloud_auth -> Credentials  |
    | Scope | openid |
    | Groups claim | resource_access.nextcloud-auth.roles |

4. If you trust your configuration, enable "Standard-Login ausblenden".
