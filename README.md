# Nextcloud Helm chart

![Version: 4.1.0](https://img.shields.io/badge/Version-4.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 30.0.5-fpm-alpine](https://img.shields.io/badge/AppVersion-30.0.5--fpm--alpine-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy [Nextcloud](https://nextcloud.com/) on Kubernetes.
Nextcloud is a self-hosted, open-source file sync and share platform that allows you to securely access and share your files, calendars, contacts, and more.

We use the official image of [Nextcloud](https://hub.docker.com/_/nextcloud/), specifically the [NGINX](https://nginx.org/) flavored version.

The chart requires the following resources:

* External resources:
    * An existing [PostgreSQL](https://www.postgresql.org/) database.
    * An existing [restic](https://restic.net/) repository accessible via sftp (if backup is enabled)
* In-cluster components:
    * [Traefik v2](https://traefik.io/traefik/) with its associated CRDs (if ingress is enabled)
    * [Argo Workflows](https://argoproj.github.io/workflows/) with its associated CRDs (if backup is enabled)

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Sven Prüfer | <sven@musmehl.de> |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry-1.docker.io/bitnamicharts | valkey | 2.2.3 |

## Installation and configuration

1. Create namespace.

    ```shell
    kubectl create namespace nextcloud
    ```

2. Create a database user and a database in the internal database using the [script](./database-setup/database-setup.sql). Don't forget to modify the password.

3. Create a secret for the nextcloud database crendentials:

    ```shell
    kubectl create secret generic database-credentials \
    --from-literal=database=nextcloud \
    --from-literal=user=nextclouduser \
    --from-literal=password=CHANGEME \
    -n nextcloud
    ```

4. Create a secret for the mail credentials:

    ```shell
   kubectl create secret generic mail-credentials \
   --from-literal=host=CHANGEME \
   --from-literal=port=CHANGEME \
   --from-literal=encryptionMode=CHANGEME \
   --from-literal=username=CHANGEME \
   --from-literal=password=CHANGEME \
   --from-literal=fromLocalPart=CHANGEME \
   --from-literal=fromDomain=CHANGEME \
   --from-literal=toEmail=CHANGEME \
   -n nextcloud
   ```

5. Optionally create a secret for the valkey credentials:

    ```shell
    kubectl create secret generic nextcloud-valkey \
    --from-literal=valkey-password=CHANGEME \
    -n nextcloud
    ```

    **Note**: The sign '@' is not allowed in the password string.

6. Optionally create a secret for the metrics token:

   ```shell
   kubectl create secret generic nextcloud-metrics \
   --from-literal=token=CHANGEME \
   -n nextcloud
   ```

7. Deploy the helm chart:

   ```shell
   helm upgrade --install -n nextcloud nextcloud .
   ```

8. Change the ownership of config and data directory and create `CAN_INSTALL` inside the nextcloud container.

    ```shell
    chown www-data:www-data /var/www/html/config
    chown www-data:www-data <dataDir>
    touch /var/www/html/config/CAN_INSTALL
    ```

9. Install Nexcloud by going to the website and following instructions.

10. Handle nextcloud warnings.

11. Install apps.
    * Calendar
    * Contacts
    * Group folder
    * File access control
    * Client push
    * Social Login
    * Login Notes
    * Tasks
    * External Sites

12. Setup High Performance File Backend

    If `autoSetup` is not enabled, configure the settings manually within the Nextcloud container.

    ```shell
    su -s /bin/sh www-data
    /var/www/html/occ app:install notify_push
    /var/www/html/occ config:app:set notify_push base_endpoint --value="https://cloud.mathezirkel-augsburg.de/push"
    ```

13. Setup metrics exporter

    If `autoSetup` is not enabled, configure the settings manually within the Nextcloud container.

    ```shell
    su -s /bin/sh www-data
    /var/www/html/occ config:app:set serverinfo token --value "$NEXTCLOUD_METRICS_TOKEN"
    ```

14. Configure the Nextcloud.

## Set up backup

1. Initialize a restic repository.

    ```shell
    restic -r sftp:<ssh-alias>:<path-to-repository> init
    ```

2. Create a secret for the repository credentials.

    ```shell
    kubectl create secret generic backup-restic-credentials \
    --from-literal=password=CHANGEME \
    -n nextcloud
    ```

3. Create a secret for the SSH Key.

    ```shell
    ssh-keyscan -t rsa,ecdsa,ed25519 -H -p CHANGEME CHANGEME > /tmp/known_hosts
    ```

    ```shell
    kubectl create secret generic backup-ssh-credentials \
    --from-literal=hostname=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=user=CHANGEME \
    --from-literal=path=CHANGEME \
    --from-file=knownHosts=/tmp/known_hosts \
    --from-file=privateKey=CHANGEME \
    -n nextcloud
    ```

4. Test your backup regularly! Detailed instructions can be found [here](https://gitlab.com/mathezirkel/server/helm-nextcloud/-/tree/main/backup/).

## Update notes

**Do not use the web-based updater and do not update via occ!**

Updating is done by replacing the old images in the manifests and rollouting the updated deployments.

**It is only possible to upgrade one major version at a time.**

Since all data and configuration is stored in persistent volumes, nothing gets lost. The startup script will check for the version in the volume and the installed version of the docker image. If it finds a mismatch, it will automatically starts the upgrade process.

## Maintenance

In order to use the [occ command](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#config-commands-label) execute

```shell
su -s /bin/sh www-data
```

inside the nextcloud container.

## Values

### Backup

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| backup.cleanUpPolicy.keepDaily | int | `7` | Number of recent daily snapshots to keep |
| backup.cleanUpPolicy.keepLast | int | `3` | Number of recent snapshots to keep |
| backup.cleanUpPolicy.keepMonthly | int | `6` | Number of recent monthly snapshots to keep |
| backup.cleanUpPolicy.keepWeekly | int | `4` | Number of recent weekly snapshots to keep |
| backup.databaseDumpTempVolumeSize | string | `"10Gi"` | Size of the temporarily generated volume saving the database dumps |
| backup.enabled | bool | `true` | Enable the backup. |
| backup.failedJobsHistoryLimit | int | `2` | Number of failed jobs kept in history |
| backup.lastSuccessfulBackupThreshold | int | `24` | Maximum allowable age in hours for the most recent successful backup |
| backup.pullPolicy | string | `"IfNotPresent"` | Workflow image pull policy |
| backup.restic.credentials.repository | object | `{"key":"password","name":"backup-restic-credentials"}` | Existing secret containing the repository credentials |
| backup.restic.credentials.repository.key | string | `"password"` | Key containing the password |
| backup.restic.credentials.repository.name | string | `"backup-restic-credentials"` | Name of the secret |
| backup.restic.credentials.ssh | object | `{"hostnameKey":"hostname","knownHostsKey":"knownHosts","name":"backup-ssh-credentials","pathKey":"path","portKey":"port","privateKeyKey":"privateKey","userKey":"user"}` | Existing secret containing the SSH credentials of the backup server |
| backup.restic.credentials.ssh.hostnameKey | string | `"hostname"` | Key containing the hostname |
| backup.restic.credentials.ssh.knownHostsKey | string | `"knownHosts"` | Key containing the known_hosts file |
| backup.restic.credentials.ssh.name | string | `"backup-ssh-credentials"` | Name of the secret |
| backup.restic.credentials.ssh.pathKey | string | `"path"` | Key containing the path |
| backup.restic.credentials.ssh.portKey | string | `"port"` | Key containing the port |
| backup.restic.credentials.ssh.privateKeyKey | string | `"privateKey"` | Key containing the private SSH key |
| backup.restic.credentials.ssh.userKey | string | `"user"` | Key containing the user |
| backup.restic.image.repository | string | `"restic/restic"` | Restic image repository |
| backup.restic.image.tag | string | `"0.17.0"` | Restic image tag Same as version on backup server |
| backup.schedule.backup | string | `"0 3 * * *"` | Schedule of the backup |
| backup.schedule.check | string | `"0 11 * * *"` | Schedule of the backup check |
| backup.schedule.clean | string | `"0 19 * * 0"` | Schedule of the backup clean up |
| backup.successfulJobsHistoryLimit | int | `1` | Number of successful jobs kept in history |

### Cron

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| cron.enabled | bool | `true` | Enable Cronjob |
| cron.failedJobsHistoryLimit | int | `2` | Number of failed jobs kept in history |
| cron.schedule | string | `"*/5 * * * *"` | Schedule for the Nextcloud cronjob |
| cron.successfulJobsHistoryLimit | int | `1` | Number of successful jobs kept in history |

### Database

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| database.existingSecret.databaseKey | string | `"database"` | Key containing the database name |
| database.existingSecret.name | string | `"database-credentials"` | Name of an existing secret containing the database credentials |
| database.existingSecret.passwordKey | string | `"password"` | Key containing the user password |
| database.existingSecret.userKey | string | `"user"` | Key containing the database user |
| database.host | string | `"postgresql17.database.svc.cluster.local"` | External database service Currently only PostgreSQL is supported by this chart |
| database.port | int | `5432` | Port of the external database |
| database.prefix | string | `"oc_"` | Prefix of the nextcloud database tables |
| database.version | string | `"17.2-alpine"` | Database version |

### Deployment

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| fullnameOverride | string | `""` | String to fully override nextcloud.fullname |
| nameOverride | string | `""` | String to partially override nextcloud.fullname |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for the Nexcloud application If set to false, the default serviceAccount is used. |
| serviceAccount.name | string | nextcloud.fullname | The name of the service account to use. |

### Ingress

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.additionalMiddlewares | list | `[{"name":"security-headers","namespace":"kube-public"}]` | List of additional middlewares to apply on the ingress |
| ingress.enabled | bool | `true` | Enable the ingress |
| ingress.entryPoints | list | `["websecure"]` | List of traefik entrypoints to listen |
| ingress.host | string | `"cloud.mathezirkel-augsburg.de"` | Host the nextcloud should be available on |
| ingress.tls.certResolver | string | `"lets-encrypt"` | Name of the certResolver configured via traefik |

### Metrics

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| metrics.autoSetup | bool | `true` | Automatically setup metrics exporter |
| metrics.config.existingSecret.enabled | bool | `false` | Whether to use an existing secret for the token NOTE: When it's enabled, the previous `token` parameter is ignored |
| metrics.config.existingSecret.name | string | `"nextcloud-metrics"` | Name of an existing secret containing the token |
| metrics.config.existingSecret.tokenKey | string | `"token"` | Key containing the token |
| metrics.config.token | string | `""` | Token for the metrics exporter Defaults to a random 32-character alphanumeric string if not set |
| metrics.enabled | bool | `true` | Enable metrics exporter |
| metrics.image.pullPolicy | string | `"IfNotPresent"` | Metrics exporter image pull policy |
| metrics.image.repository | string | `"xperimental/nextcloud-exporter"` | Metrics exporter image repository |
| metrics.image.tag | string | `"0.8.0"` | Metrics exporter image tag |
| metrics.livenessProbe.enabled | bool | `true` | Enable livenessProbe |
| metrics.livenessProbe.failureThreshold | int | `3` | Failure threshold for livenessProbe |
| metrics.livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for livenessProbe |
| metrics.livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| metrics.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| metrics.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| metrics.readinessProbe.enabled | bool | `true` | Enable readinessProbe |
| metrics.readinessProbe.failureThreshold | int | `3` | Failure threshold for readinessProbe |
| metrics.readinessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for readinessProbe |
| metrics.readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| metrics.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| metrics.readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| metrics.replicaCount | int | `1` | Number of replicas to deploy |
| metrics.resources.limits.memory | string | `"250Mi"` | The memory limits for the containers |
| metrics.resources.requests.cpu | string | `"50m"` | The requested cpu for the containers |
| metrics.resources.requests.memory | string | `"125Mi"` | The requested memory for the containers |
| metrics.service.port | int | `9205` | Metrics exporter service port |
| metrics.startupProbe.enabled | bool | `true` | Enable startupProbe |
| metrics.startupProbe.failureThreshold | int | `10` | Failure threshold for startupProbe |
| metrics.startupProbe.initialDelaySeconds | int | `30` | Initial delay seconds for startupProbe |
| metrics.startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| metrics.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| metrics.startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| metrics.strategy | object | `{"rollingUpdate":{"maxSurge":1,"maxUnavailable":0},"type":"RollingUpdate"}` | Strategy used to replace old pods |
| metrics.timeout | string | `"5s"` | Timeout for getting server info document. |

### Nextcloud

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| nextcloud.config.defaultPhoneRegion | string | `"DE"` | Nextcloud config parameter `default-phone-region` |
| nextcloud.config.forwardedForHeader | list | `["HTTP_X_FORWARDED_FOR"]` | HTTP headers with the original client IP address |
| nextcloud.config.logLevel | string | `"2"` | Nextcloud logging level. Possible: 0: DEBUG, 1: INFO, 2: WARN, 3: ERROR, 4: FATAL |
| nextcloud.config.maintenanceWindowStart | int | `1` | Nextcloud config parameter `maintenance_window_start` |
| nextcloud.config.mounted | list | `["account_manager.default_property_scope.config.php","apcu.config.php","apps.config.php","database.config.php","log.config.php","maintenance-window.config.php","phone-region.config.php","redis.config.php","reverse-proxy.config.php","smtp.config.php","trashbin.config.php","trusted-domains.config.php","upgrade-disable-web.config.php"]` | Mounted configuration files for nextcloud |
| nextcloud.config.overwriteprotocol | string | `"https"` | Nextcloud config parameter `overwriteprotocol` |
| nextcloud.config.trashbinRetentionObligation | string | `"auto, 30"` | Nextcloud config parameter `trashbin_retention_obligation` |
| nextcloud.config.trustedDomains | list | `[]` | List of trusted domains @default: [localhost, nginx, ingress] The default values must not be included in the list. |
| nextcloud.config.trustedProxies | list | `["10.42.0.0/24"]` | Listed of trusted proxy IPs @default: [127.0.0.1] The default values must not be included in the list. |
| nextcloud.hooks | object | `{"before-starting":[],"post-installation":[],"post-upgrade":[],"pre-installation":[],"pre-upgrade":[]}` | Hooks for auto configuration ref: https://github.com/nextcloud/docker?tab=readme-ov-file#auto-configuration-via-hook-folders |
| nextcloud.image.pullPolicy | string | `"IfNotPresent"` | Nextcloud image pull policy |
| nextcloud.image.repository | string | `"nextcloud"` | Nextcloud image repository |
| nextcloud.image.tag | string | Chart.appVersion | Nextcloud image tag |
| nextcloud.livenessProbe.enabled | bool | `true` | Enable livenessProbe on Nextcloud containers |
| nextcloud.livenessProbe.failureThreshold | int | `3` | Failure threshold for livenessProbe |
| nextcloud.livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for livenessProbe |
| nextcloud.livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| nextcloud.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| nextcloud.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| nextcloud.phpConfig.maxChildren | string | `"115"` | PHP - The maximum number of child processes allowed to be spawned. |
| nextcloud.phpConfig.maxRequests | string | `"0"` | PHP - Maximum number of requests |
| nextcloud.phpConfig.maxSpareServers | string | `"86"` | PHP - The maximum number of idle child processes PHP-FPM will create. If there are more child processes available than this value, then some will be killed off. |
| nextcloud.phpConfig.memoryLimit | string | `"4G"` | PHP - Memory limit |
| nextcloud.phpConfig.minSpareServers | string | `"28"` | PHP - The minimum number of idle child processes PHP-FPM will create. More are created if fewer than this number are available. |
| nextcloud.phpConfig.mounted | list | `["zz-docker.conf"]` | List of PHP configuration files to be mounted |
| nextcloud.phpConfig.startServers | string | `"57"` | PHP - The number of child processes to start when PHP-FPM starts. |
| nextcloud.phpConfig.uploadLimit | string | `"10G"` | PHP - Upload limit |
| nextcloud.readinessProbe.enabled | bool | `true` | Enable readinessProbe on Nextcloud containers |
| nextcloud.readinessProbe.failureThreshold | int | `3` | Failure threshold for readinessProbe |
| nextcloud.readinessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for readinessProbe |
| nextcloud.readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| nextcloud.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| nextcloud.readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| nextcloud.replicaCount | int | `1` | Number of Nextcloud replicas to deploy |
| nextcloud.resources.limits.memory | string | `"5Gi"` | The memory limits for the Nextcloud containers |
| nextcloud.resources.requests.cpu | string | `"500m"` | The requested cpu for the Nextcloud containers |
| nextcloud.resources.requests.memory | string | `"4Gi"` | The requested memory for the Nextcloud containers |
| nextcloud.service.port | int | `9000` | Nextcloud service port |
| nextcloud.startupProbe.enabled | bool | `true` | Enable startupProbe on Nextcloud containers |
| nextcloud.startupProbe.failureThreshold | int | `60` | Failure threshold for startupProbe |
| nextcloud.startupProbe.initialDelaySeconds | int | `10` | Initial delay seconds for startupProbe |
| nextcloud.startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| nextcloud.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| nextcloud.startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| nextcloud.strategy | object | `{"type":"Recreate"}` | Strategy used to replace old pods IMPORTANT: use with care, it is suggested to leave as that for upgrade purposes |

### NGINX

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| nginx.image.pullPolicy | string | `"IfNotPresent"` | Nginx image pull policy |
| nginx.image.repository | string | `"nginx"` | Nginx image repository |
| nginx.image.tag | string | `"1.27.3-alpine"` | Nginx image tag |
| nginx.livenessProbe.enabled | bool | `true` | Enable livenessProbe on Nginx containers |
| nginx.livenessProbe.failureThreshold | int | `3` | Failure threshold for livenessProbe |
| nginx.livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for livenessProbe |
| nginx.livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| nginx.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| nginx.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| nginx.readinessProbe.enabled | bool | `true` | Enable readinessProbe on Nginx containers |
| nginx.readinessProbe.failureThreshold | int | `3` | Failure threshold for readinessProbe |
| nginx.readinessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for readinessProbe |
| nginx.readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| nginx.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| nginx.readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| nginx.replicaCount | int | `1` | Number of Nginx replicas to deploy |
| nginx.resources.limits.memory | string | `"256Mi"` | The memory limits for the Nginx containers |
| nginx.resources.requests.cpu | string | `"100m"` | Enable startupProbe on Nginx containers |
| nginx.resources.requests.memory | string | `"128Mi"` | The requested memory for the Nginx containers |
| nginx.service.port | int | `80` | Nginx service port |
| nginx.startupProbe.enabled | bool | `true` | Enable startupProbe on Nginx containers |
| nginx.startupProbe.failureThreshold | int | `60` | Failure threshold for startupProbe |
| nginx.startupProbe.initialDelaySeconds | int | `30` | Initial delay seconds for startupProbe |
| nginx.startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| nginx.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| nginx.startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| nginx.strategy | object | `{"rollingUpdate":{"maxSurge":1,"maxUnavailable":0},"type":"RollingUpdate"}` | Strategy used to replace old pods |

### Notify Push

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| notifyPush.application.binaryName | string | `"notify_push"` | Name of the executed binary |
| notifyPush.application.mountPath | string | `"/srv/app/"` | The path the binary will be mounted inside the container |
| notifyPush.application.subPath | string | `"custom_apps/notify_push/bin/x86_64"` | Location of the executed binary inside the Nextcloud web volume |
| notifyPush.autoSetup | bool | `true` | Automatically setup high-performance file backend |
| notifyPush.enabled | bool | `true` | Enable high-performance file backend |
| notifyPush.image.pullPolicy | string | `"IfNotPresent"` | Image pull policy |
| notifyPush.image.repository | string | `"alpine"` | Image repository |
| notifyPush.image.tag | string | `"3.20.3"` | Image tag |
| notifyPush.livenessProbe.enabled | bool | `true` | Enable livenessProbe |
| notifyPush.livenessProbe.failureThreshold | int | `3` | Failure threshold for livenessProbe |
| notifyPush.livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for livenessProbe |
| notifyPush.livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| notifyPush.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| notifyPush.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| notifyPush.metrics.port | int | `9867` | Notify-Push metrics port |
| notifyPush.readinessProbe.enabled | bool | `true` | Enable readinessProbe |
| notifyPush.readinessProbe.failureThreshold | int | `3` | Failure threshold for readinessProbe |
| notifyPush.readinessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for readinessProbe |
| notifyPush.readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| notifyPush.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| notifyPush.readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| notifyPush.replicaCount | int | `1` | Number of replicas to deploy |
| notifyPush.resources.limits.memory | string | `"500Mi"` | The memory limits for the containers |
| notifyPush.resources.requests.cpu | string | `"100m"` | The requested cpu for the containers |
| notifyPush.resources.requests.memory | string | `"250Mi"` | The requested memory for the containers |
| notifyPush.service.port | int | `7867` | Notify-Push service port |
| notifyPush.startupProbe.enabled | bool | `true` | Enable startupProbe |
| notifyPush.startupProbe.failureThreshold | int | `6` | Failure threshold for startupProbe |
| notifyPush.startupProbe.initialDelaySeconds | int | `10` | Initial delay seconds for startupProbe |
| notifyPush.startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| notifyPush.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| notifyPush.startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| notifyPush.strategy | object | `{"type":"Recreate"}` | Strategy used to replace old pods |

### Persistence

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| persistence.data.accessMode | string | `"ReadWriteOnce"` | PVC Access Mode for Nextcloud data volume |
| persistence.data.dataDir | string | `"/home/www-data/data"` | Location of the data inside the nextcloud container |
| persistence.data.size | string | `"300Gi"` | PVC Storage Request for Nextcloud data volume |
| persistence.data.storageClassName | string | `"retain-local-path"` | PVC StorageClass for Nextcloud data volume |
| persistence.web.accessMode | string | `"ReadWriteOnce"` | PVC Access Mode for Nextcloud web volume |
| persistence.web.size | string | `"10Gi"` | PVC Storage Request for Nextcloud web volume |
| persistence.web.storageClassName | string | `"retain-local-path"` | PVC StorageClass for Nextcloud web volume |

### SMTP

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| smtp | object | `{"existingSecret":{"encryptionModeKey":"encryptionMode","fromDomainKey":"fromDomain","fromLocalPartKey":"fromLocalPart","hostKey":"host","name":"mail-credentials","passwordKey":"password","portKey":"port","toEmailKey":"toEmail","usernameKey":"username"},"nextcloud":{"enabled":true}}` | SMTP settings for Nextcloud and Backup workflow |
| smtp.existingSecret.encryptionModeKey | string | `"encryptionMode"` | Key containing the encryption method (Possible values: `ssl` or `""` (None/startls)) |
| smtp.existingSecret.fromDomainKey | string | `"fromDomain"` | Key containing the domain for the 'from' field in the emails. |
| smtp.existingSecret.fromLocalPartKey | string | `"fromLocalPart"` | Key containing the local-part for the 'from' field in the emails. |
| smtp.existingSecret.hostKey | string | `"host"` | Key containing the hostname of the SMTP server |
| smtp.existingSecret.name | string | `"mail-credentials"` | Name of an existing secret containing mail credentials |
| smtp.existingSecret.passwordKey | string | `"password"` | Key containing the password for authentication |
| smtp.existingSecret.portKey | string | `"port"` | Key containing the SMTP port |
| smtp.existingSecret.toEmailKey | string | `"toEmail"` | Key containing an email address to receive backup notification |
| smtp.existingSecret.usernameKey | string | `"username"` | Key containing username for authentication |
| smtp.nextcloud.enabled | bool | `true` | Enable external mail server for Nextcloud |

### Valkey

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| valkey.architecture | string | `"standalone"` | Valkey architecture. Allowed values: `standalone` or `replication` |
| valkey.auth.enabled | bool | `true` | Enable password authentication |
| valkey.auth.existingSecret | string | `"nextcloud-valkey"` | The name of an existing secret with Valkey credentials NOTE: If managed, this has to be "nextcloud-valkey" |
| valkey.auth.existingSecretPasswordKey | string | `"valkey-password"` | Password key to be retrieved from existing secret NOTE: If managed, this has to be "valkey-password" |
| valkey.auth.managed | bool | `true` | Create password automatically (random 32-character alphanumeric string) |
| valkey.enabled | bool | `true` | Enable Valkey |
| valkey.primary.livenessProbe.enabled | bool | `true` | Enable livenessProbe on Valkey primary nodes |
| valkey.primary.livenessProbe.failureThreshold | int | `5` | Failure threshold for livenessProbe |
| valkey.primary.livenessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for livenessProbe |
| valkey.primary.livenessProbe.periodSeconds | int | `5` | Period seconds for livenessProbe |
| valkey.primary.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| valkey.primary.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| valkey.primary.persistence.enabled | bool | `true` | Enable persistence on Valkey primary nodes using Persistent Volume Claims |
| valkey.primary.readinessProbe.enabled | bool | `true` | Enable readinessProbe on Valkey primary nodes |
| valkey.primary.readinessProbe.failureThreshold | int | `5` | Failure threshold for readinessProbe |
| valkey.primary.readinessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for readinessProbe |
| valkey.primary.readinessProbe.periodSeconds | int | `5` | Period seconds for readinessProbe |
| valkey.primary.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| valkey.primary.readinessProbe.timeoutSeconds | int | `1` | Timeout seconds for readinessProbe |
| valkey.primary.resources.limits.memory | string | `"192Mi"` | The memory limits for the Valkey containers |
| valkey.primary.resources.requests.cpu | string | `"100m"` | The requested cpu for the Valkey containers |
| valkey.primary.resources.requests.memory | string | `"128Mi"` | The requested memory for the Valkey containers |
| valkey.primary.service.ports.valkey | int | `6379` | Valkey primary service port |
| valkey.primary.startupProbe.enabled | bool | `true` | Enable startupProbe on Valkey primary nodes |
| valkey.primary.startupProbe.failureThreshold | int | `5` | Failure threshold for startupProbe |
| valkey.primary.startupProbe.initialDelaySeconds | int | `20` | Initial delay seconds for startupProbe |
| valkey.primary.startupProbe.periodSeconds | int | `5` | Period seconds for startupProbe |
| valkey.primary.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| valkey.primary.startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |

## License

This repository is based on the work from the [Nextcloud helm chart](https://github.com/nextcloud/helm) which is licensed under the [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.txt).
The modifications and contents of this repository are licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
