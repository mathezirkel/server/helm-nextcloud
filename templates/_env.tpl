{{/*
Create environment variables used to configure the nextcloud container as well as the cron container.
*/}}
{{- define "nextcloud.env" -}}
# Database
- name: DB_HOST
  value: {{ .Values.database.host | quote }}
- name: DB_PORT
  value: {{ .Values.database.port | toString | quote }}
- name: DB_USER
  valueFrom:
    secretKeyRef:
      name: {{ .Values.database.existingSecret.name }}
      key: {{ .Values.database.existingSecret.userKey }}
- name: DB_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.database.existingSecret.name }}
      key: {{ .Values.database.existingSecret.passwordKey }}
- name: DB_NAME
  valueFrom:
    secretKeyRef:
      name: {{ .Values.database.existingSecret.name }}
      key: {{ .Values.database.existingSecret.databaseKey }}
- name: DB_PREFIX
  value: {{ .Values.database.prefix }}
# Redis
{{- if .Values.valkey.enabled }}
- name: REDIS_HOST
  value: {{ template "nextcloud.valkey.fullname" . }}-primary
- name: REDIS_HOST_PORT
  value: {{ .Values.valkey.primary.service.ports.valkey | quote }}
{{- if .Values.valkey.auth.enabled }}
{{- if and .Values.valkey.auth.existingSecret .Values.valkey.auth.existingSecretPasswordKey }}
- name: REDIS_HOST_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.valkey.auth.existingSecret }}
      key: {{ .Values.valkey.auth.existingSecretPasswordKey }}
{{- else if not .Values.valkey.auth.password }}
- name: REDIS_HOST_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ template "nextcloud.valkey.fullname" . }}
      key: valkey-password
{{- else }}
- name: REDIS_HOST_PASSWORD
  value: {{ .Values.redis.auth.password }}
{{- end }}
{{- end }}
{{- end }}
# Metrics
{{- if .Values.metrics.enabled }}
- name: NEXTCLOUD_METRICS_TOKEN
{{- if .Values.metrics.config.existingSecret.enabled }}
  valueFrom:
    secretKeyRef:
      name: {{ .Values.metrics.config.existingSecret.name }}
      key: {{ .Values.metrics.config.existingSecret.tokenKey }}
{{- else if not .Values.metrics.config.token }}
  valueFrom:
    secretKeyRef:
      name: {{ printf "%s-metrics" (include "nextcloud.fullname" .) }}
      key: token
{{- else }}
  value: {{ .Values.metrics.config.token }}
{{- end }}
{{- end }}
# PHP_VALUES
- name: PHP_MEMORY_LIMIT
  value: {{ .Values.nextcloud.phpConfig.memoryLimit | quote }}
- name: PHP_UPLOAD_LIMIT
  value: {{ .Values.nextcloud.phpConfig.uploadLimit | quote }}
- name: PM_MAX_CHILDREN
  value: {{ .Values.nextcloud.phpConfig.maxChildren | quote }}
- name: PM_START_SERVERS
  value: {{ .Values.nextcloud.phpConfig.startServers | quote }}
- name: PM_MIN_SPARE_SERVERS
  value: {{ .Values.nextcloud.phpConfig.minSpareServers | quote }}
- name: PM_MAX_SPARE_SERVERS
  value: {{ .Values.nextcloud.phpConfig.maxSpareServers | quote }}
- name: PM_MAX_REQUESTS
  value: {{ .Values.nextcloud.phpConfig.maxRequests | quote }}
# Logging
- name: LOG_LEVEL
  value: {{ .Values.nextcloud.config.logLevel | quote }}
# Trusted domains
- name: TRUSTED_DOMAINS
  value: {{ concat .Values.nextcloud.config.trustedDomains (list .Values.ingress.host (printf "%s-nginx" (include "nextcloud.fullname" .)) "localhost") | join " " }}
# Reverse Proxy
- name: OVERWRITEPROTOCOL
  value: {{ .Values.nextcloud.config.overwriteprotocol | quote }}
{{- with .Values.nextcloud.config.forwardedForHeader }}
- name: FORWARDED_FOR_HEADERS
  value: {{ . | join " "}}
{{- end }}
# Trusted Proxies
- name: TRUSTED_PROXIES
  value: {{ append .Values.nextcloud.config.trustedProxies "127.0.0.1" | join " " }}
# Default phone region
- name: DEFAULT_PHONE_REGION
  value: {{ .Values.nextcloud.config.defaultPhoneRegion | quote }}
# Trash bin retention obligation
- name: TRASHBIN_RETENTION_OBLIGATION
  value: {{ .Values.nextcloud.config.trashbinRetentionObligation | quote }}
# Maintenance Window
- name: MAINTENANCE_WINDOW_START
  value: {{ .Values.nextcloud.config.maintenanceWindowStart | quote }}
# SMTP
{{- if .Values.smtp.nextcloud.enabled }}
- name: SMTP_HOST
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.existingSecret.name }}
      key: {{ .Values.smtp.existingSecret.hostKey }}
- name: SMTP_PORT
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.existingSecret.name }}
      key: {{ .Values.smtp.existingSecret.portKey }}
- name: SMTP_SECURE
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.existingSecret.name }}
      key: {{ .Values.smtp.existingSecret.encryptionModeKey }}
- name: SMTP_NAME
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.existingSecret.name }}
      key: {{ .Values.smtp.existingSecret.usernameKey }}
- name: SMTP_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.existingSecret.name }}
      key: {{ .Values.smtp.existingSecret.passwordKey }}
- name: MAIL_FROM_ADDRESS
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.existingSecret.name }}
      key: {{ .Values.smtp.existingSecret.fromLocalPartKey }}
- name: MAIL_DOMAIN
  valueFrom:
    secretKeyRef:
      name: {{ .Values.smtp.existingSecret.name }}
      key: {{ .Values.smtp.existingSecret.fromDomainKey }}
{{- end }}
{{- end }}
