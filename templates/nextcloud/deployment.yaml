apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "nextcloud.fullname" . }}
  namespace: {{ .Release.Namespace }}
  labels:
    app.kubernetes.io/name: {{ include "nextcloud.fullname" . }}
    {{- include "nextcloud.labels" . | nindent 4 }}
    app.kubernetes.io/component: nextcloud
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "nextcloud.fullname" . }}
      {{- include "nextcloud.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: nextcloud
  replicas: {{ .Values.nextcloud.replicaCount }}
  strategy:
    {{- with .Values.nextcloud.strategy }}
        {{- toYaml . | nindent 4 }}
    {{- end }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "nextcloud.fullname" . }}
        {{- include "nextcloud.labels" . | nindent 8 }}
        app.kubernetes.io/component: nextcloud
        app.kubernetes.io/version: {{ (.Values.nextcloud.image.tag | default .Chart.AppVersion) | quote }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/nextcloud/config.yaml") . | sha256sum }}
        checksum/php-config: {{ include (print $.Template.BasePath "/nextcloud/php-config.yaml") . | sha256sum }}
        checksum/hooks: {{ include (print $.Template.BasePath "/nextcloud/hooks.yaml") . | sha256sum }}
        {{- if and .Values.notifyPush.enabled .Values.notifyPush.autoSetup }}
        checksum/notifyPush: {{ include (print $.Template.BasePath "/notify-push/configmap.yaml") . | sha256sum }}
        {{- end }}
        {{- if and .Values.metrics.enabled .Values.metrics.autoSetup }}
        checksum/metrics: {{ include (print $.Template.BasePath "/metrics/configmap.yaml") . | sha256sum }}
        {{- end }}
    spec:
      serviceAccountName: {{ include "nextcloud.serviceAccountName" . }}
      initContainers:
        - name: postgresql-isready
          image: {{ printf "postgres:%s" .Values.database.version }}
          env:
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.database.existingSecret.name }}
                  key: {{ .Values.database.existingSecret.userKey }}
            - name: POSTGRES_HOST
              value: {{ .Values.database.host }}
          command:
            - "sh"
            - "-c"
            - "until pg_isready -h ${POSTGRES_HOST} -U ${POSTGRES_USER} ; do sleep 2 ; done"
      containers:
        - name: {{ include "nextcloud.fullname" . }}
          image: "{{ .Values.nextcloud.image.repository }}:{{ .Values.nextcloud.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.nextcloud.image.pullPolicy }}
          ports:
            - name: php
              containerPort: 9000
              protocol: TCP
          env:
            {{- include "nextcloud.env" . | nindent 12 }}
          volumeMounts:
            {{- include "nextcloud.volumeMounts" . | nindent 12 }}
            {{- range $hook, $scripts := .Values.nextcloud.hooks }}
            {{- range $script := $scripts }}
            - name: nextcloud-hooks
              subPath: {{ $script }}
              mountPath: /docker-entrypoint-hooks.d/{{ $hook }}/{{ $script }}
            {{- end }}
            {{- end }}
            {{- if and .Values.notifyPush.enabled .Values.notifyPush.autoSetup }}
            - name: nextcloud-notify-push
              subPath: notify-push.sh
              mountPath: /docker-entrypoint-hooks.d/before-starting/notify-push.sh
            {{- end }}
            {{- if and .Values.metrics.enabled .Values.metrics.autoSetup }}
            - name: nextcloud-metrics
              subPath: metrics.sh
              mountPath: /docker-entrypoint-hooks.d/before-starting/metrics.sh
            {{- end }}
          resources:
            {{- toYaml .Values.nextcloud.resources | nindent 12 }}
          {{- if .Values.nextcloud.startupProbe.enabled }}
          startupProbe:
            {{- omit .Values.nextcloud.startupProbe "enabled" | toYaml | nindent 12 }}
            tcpSocket:
              port: php
          {{- end }}
          {{- if .Values.nextcloud.readinessProbe.enabled }}
          readinessProbe:
            {{- omit .Values.nextcloud.readinessProbe "enabled" | toYaml | nindent 12 }}
            tcpSocket:
              port: php
          {{- end }}
          {{- if .Values.nextcloud.livenessProbe.enabled }}
          livenessProbe:
            {{- omit .Values.nextcloud.livenessProbe "enabled" | toYaml | nindent 12 }}
            tcpSocket:
              port: php
          {{- end }}
      volumes:
        {{- include "nextcloud.volumes" . | nindent 8 }}
        - name: nextcloud-hooks
          configMap:
            name: {{ printf "%s-hooks" (include "nextcloud.fullname" .) }}
            defaultMode: 0o755
        {{- if and .Values.notifyPush.enabled .Values.notifyPush.autoSetup }}
        - name: nextcloud-notify-push
          configMap:
            name: {{ printf "%s-notify-push" (include "nextcloud.fullname" .) }}
            defaultMode: 0o755
        {{- end }}
        {{- if and .Values.metrics.enabled .Values.metrics.autoSetup }}
        - name: nextcloud-metrics
          configMap:
            name: {{ printf "%s-metrics" (include "nextcloud.fullname" .) }}
            defaultMode: 0o755
        {{- end }}
      # Will mount configuration files as www-data (id: 82) for nextcloud
      securityContext:
        fsGroup: 82
