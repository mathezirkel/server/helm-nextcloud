{{/*
Create volume mounts for the nextcloud container as well as the cron container.
*/}}
{{- define "nextcloud.volumeMounts" -}}
- name: nextcloud-web
  mountPath: /var/www/html
- name: nextcloud-data
  mountPath: {{ .Values.persistence.data.dataDir }}
{{- range $file := .Values.nextcloud.config.mounted }}
- name: nextcloud-config
  subPath: {{ $file }}
  mountPath: /var/www/html/config/{{ $file }}
{{- end }}
{{- range $file := .Values.nextcloud.phpConfig.mounted }}
- name: nextcloud-php-config
  subPath: {{ $file }}
  mountPath: /usr/local/etc/php-fpm.d/{{ $file }}
{{- end }}
{{- end }}

{{/*
Create volume for the nextcloud container as well as the cron container.
*/}}
{{- define "nextcloud.volumes" -}}
- name: nextcloud-web
  persistentVolumeClaim:
    claimName: {{ printf "%s-web" (include "nextcloud.fullname" .) }}
- name: nextcloud-data
  persistentVolumeClaim:
    claimName: {{ printf "%s-data" (include "nextcloud.fullname" .) }}
- name: nextcloud-config
  configMap:
    name: {{ printf "%s-config" (include "nextcloud.fullname" .) }}
- name: nextcloud-php-config
  configMap:
    name: {{ printf "%s-php-config" (include "nextcloud.fullname" .) }}
{{- end }}
